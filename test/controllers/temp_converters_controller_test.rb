require 'test_helper'

class TemperatureConvertersControllerTest < ActionDispatch::IntegrationTest
  test 'should get new' do
    get new_temp_converter_url
    assert_response :success
  end

  test 'should converter celsius' do
    # Stub API integration method
    Api.expects(:converter_request).returns(c: '38', f: '100.4')

    post temp_converters_path, params: { celsius: '38', fahrenheit: '' }

    assert_redirected_to new_temp_converter_path(c: 38, f: 100.4)
  end

  test 'should converter fahrenheit' do
    # Stub API integration method
    Api.expects(:converter_request).returns(c: '38', f: '100.4')

    post temp_converters_path, params: { celsius: '', fahrenheit: '100.4' }

    assert_redirected_to new_temp_converter_path(c: 38, f: 100.4)
  end
end

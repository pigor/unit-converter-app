require 'test_helper'

class DistanceConvertersControllerTest < ActionDispatch::IntegrationTest
  test 'should get new' do
    get new_distance_converter_url
    assert_response :success
  end

  test 'should converter miles' do
    # Stub API integration method
    Api.expects(:converter_request).returns(miles: '1', km: '1.60934')

    post distance_converters_path, params: { miles: '1', km: '' }

    assert_redirected_to new_distance_converter_path(miles: 1, km: 1.60934)
  end

  test 'should converter km' do
    # Stub API integration method
    Api.expects(:converter_request).returns(miles: '0.6213727366498067', km: '1')

    post distance_converters_path, params: { miles: '', km: '1' }

    assert_redirected_to new_distance_converter_path(miles: 0.6213727366498067, km: 1)
  end
end

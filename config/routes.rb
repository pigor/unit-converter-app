Rails.application.routes.draw do
  resources :unit_converters, only: [:index]
  resources :temp_converters, only: [:new, :create]
  resources :distance_converters, only: [:new, :create]

  root to: "unit_converters#index"
end

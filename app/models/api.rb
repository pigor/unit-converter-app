class Api
  def self.converter_request(url, params = {})
    response = RestClient.get url, params: params
    JSON.parse(response.body)
  end
end
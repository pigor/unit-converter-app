class DistanceConvertersController < ApplicationController
  def new
    @units = {
      column_1: :miles,
      value_1: params[:miles],
      column_2: :km,
      value_2: params[:km]
    }
  end

  def create
    result = Api.converter_request(
      ENV['DISTANCE_API'],
      miles: params[:miles],
      km: params[:km]
    )

    redirect_to new_distance_converter_path(result)
  end
end

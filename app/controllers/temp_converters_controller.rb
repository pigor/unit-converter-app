class TempConvertersController < ApplicationController
  def new
    @units = {
      column_1: :celsius,
      value_1: params[:c],
      column_2: :fahrenheit,
      value_2: params[:f]
    }
  end

  def create
    result = Api.converter_request(
      ENV['TEMP_API'],
      c: params[:celsius],
      f: params[:fahrenheit]
    )

    redirect_to new_temp_converter_path(result)
  end
end
